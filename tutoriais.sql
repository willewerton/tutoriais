-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26-Ago-2019 às 20:56
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tutoriais`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `flows`
--

CREATE TABLE `flows` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `flows`
--

INSERT INTO `flows` (`id`, `name`, `url`, `created_at`, `updated_at`) VALUES
(1, 'Fluxo 1', 'http://127.0.0.1:8000/home', '2019-07-16 17:55:34', '2019-07-16 17:55:34'),
(2, 'Fluxo 2', 'http://127.0.0.1:8000/home', '2019-07-16 20:34:56', '2019-07-16 20:34:56'),
(3, 'Fluxo 3', 'http://127.0.0.1:8000/home', '2019-07-16 20:48:56', '2019-07-16 20:48:56'),
(4, 'Fluxo 4', 'http://127.0.0.1:8000/home', '2019-07-16 20:48:57', '2019-07-16 20:48:57'),
(5, 'Fluxo 5', 'http://127.0.0.1:8000/home', '2019-07-16 21:01:23', '2019-07-16 21:01:23'),
(6, 'Fluxo 6', 'http://127.0.0.1:8000/home', '2019-07-16 21:01:56', '2019-07-16 21:01:56'),
(7, 'Fluxo 7', 'http://127.0.0.1:8000/home', '2019-07-16 21:08:46', '2019-07-16 21:08:46'),
(8, 'Fluxo 8', 'http://127.0.0.1:8000/home', '2019-07-16 21:09:12', '2019-07-16 21:09:12'),
(9, 'Fluxo 9', 'http://127.0.0.1:8000/home', '2019-07-16 21:10:09', '2019-07-16 21:10:09'),
(10, 'Fluxo 10', 'http://127.0.0.1:8000/home', '2019-07-16 21:19:36', '2019-07-16 21:19:36'),
(11, 'Fluxo 11', 'http://127.0.0.1:8000/home', '2019-07-16 21:56:46', '2019-07-16 21:56:46'),
(12, 'Fluxo 12', 'http://127.0.0.1:8000/home', '2019-07-18 22:45:44', '2019-07-18 22:45:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_07_11_135556_create_flows_table', 1),
(4, '2019_07_11_135852_create_steps_table', 1),
(5, '2019_07_11_140923_create_step_question_flow_table', 1),
(6, '2019_08_08_175841_create_step_msgs_table', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `steps`
--

CREATE TABLE `steps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `flow_id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL,
  `order` int(10) UNSIGNED NOT NULL,
  `video_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `question_title` varchar(195) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `question_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `question_img_subtitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification_img` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification_content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `steps`
--

INSERT INTO `steps` (`id`, `flow_id`, `type`, `order`, `video_title`, `video_url`, `question_title`, `question_img`, `question_img_subtitle`, `notification_img`, `notification_title`, `notification_content`, `created_at`, `updated_at`) VALUES
(2, 10, 3, 0, 'VIsão Geral', 'https://www.youtube.com/watch?v=aHQ3XhBRVgY', NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-31 19:41:49', '2019-07-31 19:41:49'),
(3, 12, 3, 0, 'Seja Bem vindo!', 'https://www.youtube.com/watch?v=aHQ3XhBRVgY', NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-31 19:59:28', '2019-07-31 19:59:28'),
(4, 9, 3, 1, 'Seja Bem vindo!', 'https://www.youtube.com/embed/aHQ3XhBRVgY', NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-31 20:04:25', '2019-07-31 20:04:25'),
(5, 11, 2, 2, NULL, NULL, 'segsergsergsergserg', 'http://127.0.0.1:8000/img/avatar-man.jpg', 'sdghsdgsegseg', NULL, NULL, NULL, '2019-07-31 21:11:26', '2019-07-31 21:11:26'),
(7, 11, 3, 4, 'Olá, Bem vindos ao educamuuuundo!', 'https://www.youtube.com/embed/Zs0hLEHAoSs', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-05 17:46:36', '2019-08-05 17:46:36'),
(8, 11, 3, 5, 'Testeaaaaaaaaaa', 'https://www.youtube.com/embed/_10j89XOYKk', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-07 22:23:12', '2019-08-07 22:40:39'),
(9, 9, 2, 2, NULL, NULL, 'ahahahah', 'http://127.0.0.1:8000/img/avatar-woman.jpg', 'hahahahah', NULL, NULL, NULL, '2019-08-08 20:23:25', '2019-08-08 20:24:28'),
(11, 10, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-21 21:24:22', '2019-08-21 21:24:22'),
(12, 11, 1, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-21 21:29:52', '2019-08-22 17:31:59'),
(15, 11, 1, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-22 17:36:49', '2019-08-22 17:36:49'),
(19, 12, 4, 2, NULL, NULL, NULL, NULL, NULL, 'http://127.0.0.1:8000/storage/notification_20190826165827_1566838707.png', 'Testeeeeeeeeeeeeee', '\\dc\\sdcszdcddvdd', '2019-08-26 19:58:27', '2019-08-26 20:43:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `step_msgs`
--

CREATE TABLE `step_msgs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `step_id` int(10) UNSIGNED NOT NULL,
  `msg_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `msg_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `msg_content` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `step_msgs`
--

INSERT INTO `step_msgs` (`id`, `step_id`, `msg_path`, `msg_title`, `msg_content`, `created_at`, `updated_at`) VALUES
(4, 11, 'div.flex-center div.content div.title', 'eeefe', 'fefefefefeffe', '2019-08-21 21:24:22', '2019-08-21 21:24:22'),
(5, 11, 'div.flex-center div.content div.links a#lara', 'efefefefefe', 'efeffefefef', '2019-08-21 21:24:22', '2019-08-21 21:24:22'),
(6, 11, 'div.flex-center div.content div.links a', 'zsgfz', 'gzdfgdfg', '2019-08-21 21:24:23', '2019-08-21 21:24:23'),
(1551, 12, 'div.flex-center div.content div.title', 'teste 018', 'zefwzfzefzesf', '2019-08-22 17:31:59', '2019-08-22 17:31:59'),
(1552, 12, 'div.flex-center div.content div.links a#lara', 'xdhxdghxfghxfg888', 'fghfghfh', '2019-08-22 17:31:59', '2019-08-22 17:31:59'),
(1553, 12, 'div.flex-center div.content div.links a', 'txhxfthxh88888', 'txfhfthfthfht', '2019-08-22 17:32:00', '2019-08-22 17:32:00'),
(1554, 12, 'div.flex-center div.content div.links a', 'zsfdf', 'dfgsdfgsfgsdf', '2019-08-22 17:32:00', '2019-08-22 17:32:00'),
(1555, 12, 'div.flex-center div.content div.title', 'teste 016', 'zefwzfzefzesf', '2019-08-22 17:32:00', '2019-08-22 17:32:00'),
(1556, 12, 'div.flex-center div.content div.links a#lara', 'xdhxdghxfghxfg', 'fghfghfh', '2019-08-22 17:32:00', '2019-08-22 17:32:00'),
(1557, 12, 'div.flex-center div.content div.links a', 'txhxfthxh', 'txfhfthfthfht', '2019-08-22 17:32:00', '2019-08-22 17:32:00'),
(1558, 12, 'div.flex-center div.content div.links a', 'zsfdf', 'dfgsdfgsfgsdf', '2019-08-22 17:32:00', '2019-08-22 17:32:00'),
(1577, 15, 'div.flex-center div.content div.title', 'teste 018999999999999', 'zefwzfzefzesf', '2019-08-22 17:36:49', '2019-08-22 17:36:49'),
(1578, 15, 'div.flex-center div.content div.links a#lara', 'xdhxdghxfghxfg888', 'fghfghfh', '2019-08-22 17:36:50', '2019-08-22 17:36:50'),
(1579, 15, 'div.flex-center div.content div.links a', 'txhxfthxh88888', 'txfhfthfthfht', '2019-08-22 17:36:50', '2019-08-22 17:36:50'),
(1580, 15, 'div.flex-center div.content div.links a', 'zsfdf', 'dfgsdfgsfgsdf', '2019-08-22 17:36:50', '2019-08-22 17:36:50'),
(1581, 15, 'div.flex-center div.content div.title', 'teste 016', 'zefwzfzefzesf', '2019-08-22 17:36:50', '2019-08-22 17:36:50'),
(1582, 15, 'div.flex-center div.content div.links a#lara', 'xdhxdghxfghxfg', 'fghfghfh', '2019-08-22 17:36:50', '2019-08-22 17:36:50'),
(1583, 15, 'div.flex-center div.content div.links a', 'txhxfthxh', 'txfhfthfthfht', '2019-08-22 17:36:50', '2019-08-22 17:36:50'),
(1584, 15, 'div.flex-center div.content div.links a', 'zsfdf', 'dfgsdfgsfgsdf', '2019-08-22 17:36:50', '2019-08-22 17:36:50'),
(1585, 15, 'div.flex-center div.content div.links a', 'dgdfgfd', 'dxfgdf', '2019-08-22 17:36:50', '2019-08-22 17:36:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `step_question_flow`
--

CREATE TABLE `step_question_flow` (
  `step_id` int(10) UNSIGNED NOT NULL,
  `flow_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `step_question_flow`
--

INSERT INTO `step_question_flow` (`step_id`, `flow_id`, `title`) VALUES
(9, 2, 'Fluxo 2'),
(9, 10, 'Fluxo 10');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `flows`
--
ALTER TABLE `flows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `steps`
--
ALTER TABLE `steps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `step_msgs`
--
ALTER TABLE `step_msgs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `flows`
--
ALTER TABLE `flows`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `steps`
--
ALTER TABLE `steps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `step_msgs`
--
ALTER TABLE `step_msgs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1586;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
