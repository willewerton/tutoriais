<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Flow;
use App\Step;

class TutorialController extends Controller
{
    public function index()
    {

        return view('welcome');
    }

    public function flows(Request $request)
    {
        $data = $request->all();
        $flows = Flow::with(['steps', 'steps.flows', 'steps.msgs'])->where('url', $data['url'])->get();

        return response()->json(['status' => true, 'flows' => $flows]);
    }

    public function addFlow(Request $request)
    {
        try {

            $countFlows = Flow::all()->count() + 1;

            $data = $request->all();

            $flow = new Flow();

            $flow->name = 'Fluxo ' . $countFlows;
            $flow->url = $data['url'];
            $flow->save();

            return response()->json(['status' => true, 'flow' => $flow]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'error' => $e->getMessage() . 'Linha: ' . $e->getLine()]);
        }
    }

    private function is_base64_encoded($data)
    {
        if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }


    public function addStep($id, Request $request)
    {
        try {

            $flow = Flow::find($id);

            $data = $request->all();

            if ($data['type'] == 1) {

                $msgs = $data['msgs'];

                unset($data['msgs']);
            }

            if ($data['type'] == 2) {

                $flowsQuestion = $data['flowsQuestion'];

                unset($data['flowsQuestion']);
            }

            if ($data['type'] == 4) {
                $fileContents = str_replace('data:image/png;base64,', '', $data['notification_img']);

                if ($this->is_base64_encoded($fileContents)) {
                    $img_name = 'notification_' . date('YmdHis') . '_' . time() . '.png';
                    Storage::disk('public')->put($img_name, base64_decode($fileContents));

                    $data['notification_img'] = url(Storage::url($img_name));
                } else {
                    unset($data['notification_img']);
                }
            }

            if ($data['id'] == 0) {

                $data['order'] = $flow->steps()->count() + 1;

                $step = $flow->steps()->create($data);
            } else {
                $flow->steps()->where('id', $data['id'])->update($data);
                $step = $flow->steps()->find($data['id']);
            }


            if ($data['type'] == 1) {
                $step->msgs()->delete();

                foreach ($msgs as $value) {

                    if (!is_null($value['msg_path']) && !is_null($value['msg_title']) && !is_null($value['msg_content'])) {

                        $step->msgs()->create($value);
                    }
                }
            }

            if ($data['type'] == 2) {
                $step->flows()->detach();

                foreach ($flowsQuestion as $value) {
                    if (!is_null($value['flow']) && !is_null($value['title'])) {
                        $step->flows()->attach($value['flow'], ['title' => $value['title']]);
                    }
                }
            }

            return response()->json(['status' => true, 'flow' => $flow]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'error' => $e->getMessage() . ' Linha: ' . $e->getLine() . ' Arquivo: ' . $e->getFile()]);
        }
    }

    public function remove ($id) {
        try {
            $flow = Flow::find($id);
            $flow->delete();

            return response()->json(['status' => true]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'error' => $e->getMessage() . ' Linha: ' . $e->getLine() . ' Arquivo: ' . $e->getFile()]);
        }
    }

    public function removeStep ($id) {
        try {
            $step = Step::find($id);
            $step->delete();

            return response()->json(['status' => true]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'error' => $e->getMessage() . ' Linha: ' . $e->getLine() . ' Arquivo: ' . $e->getFile()]);
        }
    }
}
