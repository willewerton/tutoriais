<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flow extends Model
{
    protected $fillable = [
        'name', 'url'
    ];

    public function steps()
    {
        return $this->hasMany('App\Step', 'flow_id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($step) { 
             $step->steps()->delete();      
        });
    }
}
