<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    protected $fillable = [
        'type',
        'order',
        'video_title',
        'video_url',
        'question_title',
        'question_img',
        'question_img_subtitle',
        'notification_img',
        'notification_title',
        'notification_content',
    ];
    
    public function flow(){
        return $this->belongsTo('App\Flow');
    }
    
    public function flows(){
            return $this->belongsToMany('App\Flow', 'step_question_flow', 'step_id', 'flow_id')->withPivot('title');
    }
    public function msgs(){
        return $this->hasMany('App\StepMsg', 'step_id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($step) { 
             $step->msgs()->delete();        
             $step->flows()->detach();
        });
    }
}
