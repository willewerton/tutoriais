<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StepMsg extends Model
{
    protected $table = 'step_msgs';

    protected $fillable = [
        'step_id',
        'msg_path',
        'msg_title',
        'msg_content'
    ];
}
