<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStepMsgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('step_msgs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('step_id');
            $table->string('msg_path')->nullable();
            $table->string('msg_title')->nullable();
            $table->string('msg_content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('step_msgs');
    }
}
