<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('steps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('flow_id');
            $table->unsignedTinyInteger('type');
            $table->unsignedTinyInteger('order');
            $table->string('video_title')->nullable();
            $table->string('video_url')->nullable();
            $table->string('question_title')->nullable();
            $table->string('question_img')->nullable();
            $table->string('question_img_subtitle')->nullable();            
            $table->string('notification_img')->nullable();
            $table->string('notification_title')->nullable();
            $table->text('notification_content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('steps');
    }
}
