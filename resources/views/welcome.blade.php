<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{ url('/')}}">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Tutorial</title>
    <style>
        body,
        html {
            height: 100%;
            font-family: "Open Sans";
            /* background-color: black; */
        }

        #tutorial-bar {
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            padding: 10px 0px;
            background-color: white;
            box-shadow: 0px -4px rgba(136, 136, 136, 0.2);
        }

        .select-fluxo {
            float: left;
            margin-left: 30px;
        }

        .steps {
            float: left;
            margin-top: 4px;
            margin-left: 10px;
        }

        .new-step {
            float: left;
            margin-top: 3px;
            margin-left: 30px;
        }

        .btn-new-fluxo {
            margin-top: 5px;
            float: right;
            margin-right: 30px;
        }

        .btn-new-fluxo a {
            text-decoration: none;
            color: #888888;
        }

        .preview-flow {
            margin-left: 10px;
            float: left;
        }

        .circle-span-sm {
            color: white;
            font-size: 10px;
            padding: 5px 8px;
            border-radius: 50%;
        }

        .circle-span {
            background-color: aqua;
            color: white;
            font-size: 12px;
            padding: 5px 8px;
            border-radius: 50%;
        }

        .modal-question-header {
            padding: 15px;
            background-color: rgb(30, 192, 212);
            text-align: center;
        }

        input.t-input {
            padding: 5px;
            width: 100%;
            background-color: rgba(0, 0, 0, 0);
            border: none;
            color: white;
            text-align: center;
            margin-bottom: 5px;
        }

        input.t-sm {
            font-size: 12px;
        }

        input.t-md {
            font-size: 14px;
        }

        input.p-white::placeholder {
            color: white;
        }

        input.p-white:focus,
        input.p-white:hover {
            border: 1px dashed white;
        }

        .radio-gender {
            font-size: 12px;
        }

        .radio-gender label {
            color: white;
        }

        .custom-control-label::before {
            top: 0px !important;
        }

        .custom-control-label::after {
            top: 0px !important;
        }

        .buttons-actions {
            width: 80px;
            position: absolute;
            top: 0px;
            right: -100px;
            z-index: 9999;
        }

        .flows-question {
            height: 300px;
            overflow: auto;
        }

        .flows-question span {
            font-size: 14px;
        }

        .flows-question label {
            font-size: 12px;
        }

        #modal-notification .modal-dialog{
            max-width: 702px !important;

        }

        .modal-notification-header {
            width: 700px;
            height: 280px;
        }
    </style>
</head>

<body>
    <iframe id="iframe-tuto" src="{{route('home')}}" width="100%" height="100%" onLoad="vm.updateFlows();" frameborder="0"></iframe>
    <div id="tutorial-bar">
        <div class="select-fluxo">
            <div class="form-group" style="margin: 0;">
                <select class="form-control" data-bind="
                    options: flows, 
                    optionsText: 'name', 
                    optionsValue: 'id',
                    optionsCaption:'Selecione um fluxo...',
                    value: flowActive">
                </select>
            </div>
        </div>
        <!--  ko if: flowActive -->
        <div class="steps" data-bind="foreach: steps">
            <span class="circle-span-sm mr-2"
                data-bind="click: $parent.editStep, clickBubble: false, text: ($index() + 1), attr: {style: 'background-color:' + $root.color($index) + ';cursor:pointer;'}"></span>
        </div>

        <div class="new-step">
            <button type="button" class="btn btn-sm btn-primary" id="list-steps" data-toggle="modal"
                data-target="#listStep">Novo passo</button>
        </div>
        <!-- /ko -->

        <div class="btn-new-fluxo">
            <a href="javascript:void(0);" data-bind="click: addFlow"><span class="circle-span"><i
                        class="fas fa-plus"></i></span>
                Criar novo
                fluxo</a>
        </div>
        <div style="clear: both;"></div>
    </div>

    <div class="modal fade" id="listStep" tabindex="-1" role="dialog" aria-labelledby="listStepLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <ul class="list-group">
                        <button type="button" data-bind="click: addStepMsg"
                            class="list-group-item list-group-item-action">Mensagem</button>
                        <button type="button" onclick="openModal('question')"
                            class="list-group-item list-group-item-action">Perguntas</button>
                        <button type="button" onclick="openModal('video')"
                            class="list-group-item list-group-item-action">Vídeo</button>
                        <button type="button" class="list-group-item list-group-item-action" onclick="openModal('notification')">Notificação</button>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-msg" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false"
        aria-labelledby="modal-msgLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <span data-bind="text: msgs()[indexMsg()].msg_path"></span>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" data-bind="value: msgs()[indexMsg()].msg_title"
                            id="msg_title" placeholder="Título">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" data-bind="value: msgs()[indexMsg()].msg_content"
                            id="msg_content" placeholder="Digite a mensagem aqui!"></textarea>
                    </div>

                    <hr>

                    <button type="button" class="btn btn-outline-danger float-left"
                        data-bind="click: removeStepMsg">Excluir</button>

                        <button type="button" class="btn btn-outline-info float-right"
                        data-bind="click: nextStepMsg">Próximo</button>

                    <button type="button" class="btn btn-outline-secondary float-right mr-2"
                        data-bind="click: previousStepMsg, attr: { disabled: indexMsg() <= 0 }">Anterior</button>
                    

                    <div class="clearfix"></div>

                </div>
                <div class="buttons-actions">
                    <!-- ko if: id() > 0 -->
                    <button type="button" class="btn btn-sm btn-block btn-outline-light"
                        data-dismiss="modal" data-bind="click: removeFlowStep">Excluir</button>
                    <!--/ko-->
                    <button type="button" class="btn btn-sm btn-block btn-outline-light"
                        onClick="closeModal('msg')">Cancelar</button>
                    <button type="button" class="btn btn-sm btn-block btn-primary"
                        data-bind="click: addStepMsgs">Salvar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-video" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false"
        aria-labelledby="modal-videoLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="video_title">Título do Vídeo</label>
                        <input type="text" class="form-control" data-bind="value: video_title" id="video_title"
                            placeholder="Ex. Visão Geral">
                    </div>
                    <div class="form-group">
                        <label for="video_title">URL do vídeo embutido (Youtube)</label>
                        <input type="text" class="form-control" data-bind="value: video_url" id="video_title"
                            placeholder="https://www.youtube.com/watch?v=id_do_video">
                    </div>
                    <!-- ko if: video_title && video_url -->
                    <button type="button" class="btn btn-outline-info" onclick="openModal('video-preview')"><i
                            class="far fa-eye"></i></button>
                    <!--/ko-->
                </div>
                <div class="buttons-actions">
                    <!-- ko if: id() > 0 -->
                    <button type="button" class="btn btn-sm btn-block btn-outline-light"
                        data-dismiss="modal" data-bind="click: removeFlowStep">Excluir</button>
                    <!--/ko-->
                    <button type="button" class="btn btn-sm btn-block btn-outline-light"
                        onClick="closeModal('video')">Cancelar</button>
                    <button type="button" class="btn btn-sm btn-block btn-primary"
                        data-bind="click: addVideo">Salvar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-video-preview" tabindex="-1" role="dialog"
        aria-labelledby="modal-video-previewLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h4 class="text-muted mt-4 mb-2" data-bind="text: video_title"></h4>
                    <iframe style="width:100%" height="360" frameborder="0"
                        data-bind="attr: {src: 'https://www.youtube.com/embed/' + videoId()}" allowfullscreen></iframe>

                    <button type="button" class="btn btn-outline-info mt-4" data-dismiss="modal">Ok,
                        entendi</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-question" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog"
        aria-labelledby="modal-questionLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-question-header">
                    <img data-bind="attr: {src: avatarQuestion}" width="80px" class="rounded-circle mb-2">
                    <div class="clearfix"></div>

                    <div class="custom-control custom-radio custom-control-inline radio-gender">
                        <input type="radio" id="radio-man" value="1" class="custom-control-input"
                            data-bind="checked: gender">
                        <label class="custom-control-label" for="radio-man">Homem</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline radio-gender">
                        <input type="radio" id="radio-woman" value="2" class="custom-control-input"
                            data-bind="checked: gender">
                        <label class="custom-control-label" for="radio-woman">Mulher</label>
                    </div>

                    <input type="text" class="t-input t-sm p-white" data-bind="value: question_img_subtitle"
                        id="question_img_subtitle" placeholder="Legenda da foto (nome e cargo)">
                    <input type="text" class="t-input t-md p-white" data-bind="value: question_title"
                        id="question_title" placeholder="Texto de boas vindas">
                </div>
                <div class="modal-body flows-question">
                    <span class="text-muted">Escolha o que deseja fazer</span>

                    <label class="text-muted">Selecione os fluxos que deseja vincular</label>

                    <!-- ko foreach: { data: flowsQuestion, as: 'fq' } -->
                    <div class="row mb-3 p-4 bg-light">
                        <div class="col-10">
                            <div class="form-group m-0">
                                <select class="form-control form-control-sm" data-bind="
                                        options: $parent.flows, 
                                        optionsText: 'name', 
                                        optionsValue: 'id',
                                        optionsCaption:'Selecione um fluxo...',
                                        value: $parent.flowsQuestion()[$index()].flow,
                                        event: {change: $parent.selectFlowQuestion.bind($data, $index())}">
                                </select>
                            </div>
                            <div class="form-group m-0" data-bind="if: $parent.flowsQuestion()[$index()].flow">
                                <label class="text-muted">Título público</label>
                                <input type="text" class="form-control form-control-sm"
                                    data-bind="value: $parent.flowsQuestion()[$index()].title">
                            </div>
                        </div>
                        <div class="col-2">
                            <button data-bind="if: $index() > 0, click: $parent.removeFlowQuestion" type="button"
                                class="btn btn-sm btn-link text-danger"><i class="fas fa-trash-alt"></i></button>
                        </div>
                    </div>
                    <!-- /ko -->
                    <button type="button" data-bind="click: addFlowQuestion"
                        class="btn btn-sm btn-block btn-outline-info mt-3">
                        <i class="fas fa-plus"></i>
                        Vincular outro fluxo
                    </button>
                </div>
                <div class="buttons-actions">
                    <!-- ko if: id() > 0 -->
                    <button type="button" v-if="step.id > 0" class="btn btn-sm btn-block btn-outline-light"
                        data-dismiss="modal" data-bind="click: removeFlowStep">Excluir</button>
                    <!--/ko-->
                    <button type="button" onclick="closeModal('question')"
                        class="btn btn-sm btn-block btn-outline-light">Cancelar</button>
                    <button type="button" data-bind="click: addQuestion"
                        class="btn btn-sm btn-block btn-primary">Salvar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-notification" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false"
        aria-labelledby="modal-notificationLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-notification-header" data-bind="attr: {style: 'background-image: url('+notification_img_preview()+')'}">
                    <label class="m-3 btn btn-sm btn-outline-warning text-center">                            
                        <i class="fas fa-camera"></i> <br>
                        Alterar foto<br> (700x280px) .png
                        <input type="file" data-bind="event: {change: function () {getDataImgNotification($element.files[0])}}" id="upImgNotification" class="d-none" accept=".png">
                    </label>
                </div>
                <div class="modal-body text-center">
                    <div class="form-group">
                        <input type="text" class="form-control text-center" data-bind="value: notification_title" id="video_title"
                            placeholder="Título da Notificação">
                    </div>
                    <div class="form-group">
                        <textarea data-bind="value: notification_content" cols="30" rows="4" placeholder="Escreva o texto aqui" class="form-control text-center"></textarea>
                    </div>

                    <button type="button" class="btn btn-outline-info mt-4">Ok,
                        entendi</button>
                </div>
                <div class="buttons-actions">
                    <!-- ko if: id() > 0 -->
                    <button type="button" class="btn btn-sm btn-block btn-outline-light"
                        data-dismiss="modal" data-bind="click: removeFlowStep">Excluir</button>
                    <!--/ko-->
                    <button type="button" class="btn btn-sm btn-block btn-outline-light"
                        onClick="closeModal('notification')">Cancelar</button>
                    <button type="button" class="btn btn-sm btn-block btn-primary"
                        data-bind="click: addNotification">Salvar</button>
                </div>
            </div>
        </div>
    </div>
</body>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.5.0/knockout-min.js"></script>
<script language="JavaScript" type="text/javascript">
    const headers = {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    };
    const baseUrl = $('meta[name="base-url"]').attr('content');
    const iframeEl = document.getElementById("iframe-tuto");
    const iframeDoc = iframeEl.contentDocument || iframeEl.contentWindow.document;
    const colors = [
        'rgb(232, 58, 117)',
        'rgb(30, 192, 212)',
        'rgb(77, 168, 81)',
        'rgb(227, 48, 110)',
        'rgb(253, 158, 25)'
    ];

    function openModal (id) {
        $('#listStep').modal('hide');
        $('#modal-'+ id).modal('show');
    }
    
    function closeModal (id) {
        console.log(id);
        $('#modal-'+ id).modal('hide');
        vm.resetAll();
    }

    
function ViewModel () {
    var self = this;

    self.flows = ko.observableArray([]);
    self.flowActive = ko.observable('');
    self.gender = ko.observable('1');
    self.flowsQuestion = ko.observableArray([
        {
            flow: '',
            title: ''
        }
    ]);
    
    self.id = ko.observable(0);
    self.type = ko.observable(0);
    self.order = ko.observable(0);
    self.video_title = ko.observable(null);
    self.video_url = ko.observable(null);
    self.question_title = ko.observable(null);
    self.question_img = ko.observable(null);
    self.question_img_subtitle = ko.observable(null);
    self.notification_img = ko.observable(null);
    self.notification_title = ko.observable(null);
    self.notification_content = ko.observable(null);

    self.notification_img_default = baseUrl + '/img/default.png';

    self.indexMsg = ko.observable(0);

    self.msgs = ko.observableArray([
        {
            id: 0,
            msg_path: '',
            msg_title: '',
            msg_content: '',
            url: ''
        }
    ]);
    

    self.steps = ko.pureComputed(function () {
        if (this.flowActive()) {

            return this.flows().find(f => f.id == this.flowActive()).steps;
        } else {
            return [];
        }
    }, this);

    self.avatarQuestion = ko.pureComputed(function () {
        console.log(this);
        if (this.gender() == '2') {
            return baseUrl + '/img/avatar-woman.jpg';
        }
        
        return baseUrl + '/img/avatar-man.jpg';
    }, this);

    self.videoId = ko.pureComputed(function () {
        if (this.video_url()) {

            urlArr = this.video_url().split('?v=');

            return urlArr[1];
        }
    }, this);

    self.notification_img_preview = ko.pureComputed(function () {
        if (this.notification_img()) {

            return this.notification_img();
        }

        return self.notification_img_default;
    }, this);

    self.color = function(index) {
        return colors[index() % 2];
    };

    self.resetAll = function() {
        this.gender('1');
        this.flowsQuestion([
            {
                flow: '',
                title: ''
            }
        ]);
        this.id(0);
        this.type(0);
        this.order(0);
        this.video_title(null);
        this.video_url(null);
        this.question_title(null);
        this.question_img(null);
        this.question_img_subtitle(null);
        this.notification_img(null);
        this.notification_title(null);
        this.notification_content(null);

        $('#upImgNotification').val('');
        
    };

    self.resetMsgs = function () {
        this.indexMsg(0);
        this.msgs([
            {
                id: 0,
                msg_path: '',
                msg_title: '',
                msg_content: '',
                url: ''
            }
        ]);
    };

    self.selectFlowQuestion = function(index) {
        if (this.flow) {
            let flowsQuestion = self.flowsQuestion();

            flowsQuestion[index].title = self.flows().find(f => f.id == this.flow).name;

            self.flowsQuestion(flowsQuestion);
            console.log(self.flowsQuestion());

        }
    };

    self.addFlowQuestion = function() {
        this.flowsQuestion([
            ...this.flowsQuestion(), {
            flow: '',
            title: ''
        }]);
    };

    self.removeFlowQuestion = function() {
        self.flowsQuestion.remove(this);
    };
    
    self.request = function(opts, success, fail) {
        axios({
            ...opts,
            headers
        })
        .then(function (response) {
            success(response);
        })
        .catch(function (error) {
            fail(error);
        });
    };

    self.updateFlows = function() {
        console.log();
        return new Promise ((resolve, reject) => {
            this.request({
                url: `${baseUrl}/getflows`,
                method: 'POST',
                data: {
                    url: iframeEl.contentWindow.location.href
                }
            }, (response) => {
                this.flows(response.data.flows);
                resolve()
            }, (error) => {
                reject(error);
            });
        })
    };

    self.addFlow = function() {
        self.request({
            url: `${baseUrl}/flow`,
            method: 'POST',
            data: {
                url: iframeEl.contentWindow.location.href
            }
        }, (response) => {
            self.updateFlows().then(r => {
                self.flowActive(response.data.flow.id * 1)
            });
        }, (error) => {
            console.log(error);
        });
    };

    self.addVideo = function() {
        if (!self.video_title()) {
            return alert('O título do vídeo é obrigatório.');
        }
        if (!self.video_url()) {
            return alert('A url do vídeo é obrigatória.');
        }

        self.addStep({
            id: self.id(),
            video_title: self.video_title() ,
            video_url: 'https://www.youtube.com/embed/' + self.videoId(),
            type: 3
        }, 'video');
    };

    self.addQuestion = function() {
        if (!self.question_title()) {
            return alert('O título é obrigatório.');
        }
        if (!self.question_img_subtitle()) {
            return alert('A legenda da imagem é obrigatória.');
        }
        if (!self.flowsQuestion().length || self.flowsQuestion()[0].flow === '' && self.flowsQuestion()[0].title === '') {
            return alert('Adicione os fluxos.');
        }

        self.addStep({
            id: self.id(),
            question_title: self.question_title() ,
            question_img: self.avatarQuestion() ,
            question_img_subtitle: self.question_img_subtitle(),
            flowsQuestion: self.flowsQuestion(),
            type: 2
        }, 'question');
    };

    self.editStep = function(step, event) {
        console.log(step);
        self.id(step.id);
        self.type(step.type);
        self.order(step.order);

        if (step.type === 1) {
            self.addStepMsg();
            self.msgs(step.msgs);
            self.indexMsg(0);

            openModal('msg');
        }

        if (step.type === 2) {
            self.question_title(step.question_title);
            self.question_img_subtitle(step.question_img_subtitle);

            self.gender(step.question_img.split('avatar-')[1] === 'man.jpg' ? '1' : '2');

            self.flowsQuestion(step.flows.map(f => {
                return {flow: f.id, title: f.pivot.title};
            }));

            openModal('question');
        }

        if (step.type === 3) {
            self.video_title(step.video_title);
            var id_do_video = step.video_url.split('embed/');

            self.video_url('https://www.youtube.com/watch?v=' + id_do_video[1]);
            openModal('video');          
        }

        if (step.type === 4) {
            self.notification_img(step.notification_img);
            self.notification_title(step.notification_title);
            self.notification_content(step.notification_content);
            openModal('notification');          
        }

        
    };

    self.addStep = function(data, modal, callback) {
        var cb = typeof callback === 'undefined' ? function () {} : callback;

        self.request({
            url: `${baseUrl}/flow/step/${self.flowActive()}`,
            method: 'PUT',
            data: data
        }, (response) => {
            closeModal(modal);
            self.updateFlows();
            self.resetAll();
            cb(response);
            console.log(response);
        }, (error) => {
            console.log(error);
        });
    };

    self.listenerClick = function (e) {
        e.preventDefault();

        var classes = [];

        for (let i = 0; i < e.path.length - 4; i++) {

            if (e.path[i].id) {
                var value = e.path[i].tagName.toLowerCase()+'#'+e.path[i].id;
            } else {
                let classes = e.path[i].classList.value.split(' ');
                let point = classes[0] ? '.' : '';
                var value = e.path[i].tagName.toLowerCase()+point+classes[0];
            }

            classes = [...classes, value ];            
        }

        let msgs = self.msgs();
        msgs[self.indexMsg()].msg_path = classes.slice(0).reverse().join(' ');
        msgs[self.indexMsg()].url = iframeDoc.src;

        self.msgs(msgs);       

        openModal('msg');
    };

    self.listenerMouseover = function(e){
        e.target.style.outline = '1px solid red';
        e.target.style.cursor = 'pointer';

        e.target.addEventListener('click', self.listenerClick);
    };      


    self.listenerMouseout = function(e){
        // console.log(e);
        e.target.style.outline = '';
        e.target.style.cursor = 'pointer';

        e.target.removeEventListener('click', self.listenerClick);
    };

    self.addStepMsg = function () {
        $('#listStep').modal('hide');
        var body = iframeDoc.getElementsByTagName("BODY")[0];        

        alert('Aperte Esc para sair do modo de seleção!');
        
        body.addEventListener('mouseover', self.listenerMouseover);    
        
        body.addEventListener('mouseout' , self.listenerMouseout);
        

        document.onkeydown = function(evt) {
            evt = evt || window.event;
            var isEscape = false;
            if ("key" in evt) {
                isEscape = (evt.key === "Escape" || evt.key === "Esc");
            } else {
                isEscape = (evt.keyCode === 27);
            }
            if (isEscape) {
                if (self.msgs()[0].msg_title !== '' 
                && self.msgs()[0].msg_content !== '' 
                && self.msgs()[0].msg_path !== '') {
                    if (confirm("Deseja salvar as mensagens?")) {
                        self.addStepMsgs();
                    } else {
                        self.resetMsgs();
                    }
                }
                body.removeEventListener('mouseover', self.listenerMouseover);
            }
        };
        
    };

    self.nextStepMsg = function () {
        if (self.indexMsg() == (self.msgs().length - 1)) {
            if (self.msgs()[self.indexMsg()].msg_title === ''
            || self.msgs()[self.indexMsg()].msg_content === ''
            || self.msgs()[self.indexMsg()].msg_path === '') {
                return alert('Preencha as informações corretamente!');
            }

            self.msgs([...self.msgs(), {
                id: 0,
                msg_path: '',
                msg_title: '',
                msg_content: '',
                url: ''
            }]);

            closeModal('msg');
        }

        console.log(self.indexMsg(), self.msgs());
        self.indexMsg(self.indexMsg() + 1);

    };

    self.previousStepMsg = function () {

        if (self.indexMsg() > 0) {
            self.indexMsg(self.indexMsg() - 1);
        }

    };

    self.removeStepMsg = function () {
        if (self.indexMsg() > 0) {
            self.indexMsg(self.indexMsg() - 1);
            self.msgs.remove(self);
        } else {
            if (self.msgs().length > 1) {
                self.indexMsg(self.indexMsg() + 1);
                self.msgs.remove(self);
            } else {

                self.resetMsgs();
                closeModal('msg');
            }
        }
    };

    self.addStepMsgs = function () {
        if (!self.msgs().length || self.msgs()[0].msg_title === '' 
        && self.msgs()[0].msg_url === ''
        && self.msgs()[0].msg_path === ''
        && self.msgs()[0].msg_content === ''
        && self.msgs()[0].url === '') {
            return alert('Sem nada para salvar.');
        }

        self.addStep({
            id: self.id(),
            msgs: self.msgs(),
            type: 1
        }, 'msg',
        function (response) {
            self.resetMsgs();
            var body = iframeDoc.getElementsByTagName("BODY")[0];   
            body.removeEventListener('mouseover', self.listenerMouseover);
        });
    };

    self.getDataImgNotification = function (file) {

        const reader = new FileReader();

        reader.onload = function(r){
            console.log(r.target.result)

            self.notification_img(r.target.result);

        }

        reader.readAsDataURL(file);
    };

    self.addNotification = function () {
        if (!self.notification_img()) {
            return alert('Insira uma imagem para a notificação.');
        }
        if (!self.notification_title()) {
            return alert('O título da notificação é obrigatório.');
        }
        if (!self.notification_content()) {
            return alert('O texto da notificação é obrigatório.');
        }

        self.addStep({
            id: self.id(),
            notification_img: self.notification_img() ,
            notification_title: self.notification_title() ,
            notification_content: self.notification_content() ,
            type: 4
        }, 'notification');
    };

    self.removeFlow = function () {
        if(confirm('Tem certeza disso?')) {
            self.request({
                url: `${baseUrl}/flow/remove/${self.flowActive()}`,
                method: 'DELETE',
            }, (response) => {
                self.updateFlows();
            }, (error) => {
                console.log(error);
            });
        }
    };
    
    self.removeFlowStep = function () {
        if(confirm('Tem certeza disso?')) {
            self.request({
                url: `${baseUrl}/flow/step/remove/${self.id()}`,
                method: 'DELETE',
            }, (response) => {
                self.updateFlows();
            }, (error) => {
                console.log(error);
            });
        }
    };
    
}

var vm = new ViewModel();

ko.applyBindings(vm);

vm.updateFlows();

$('.modal').on('hidden.bs.modal', function (e) {
    if (e.currentTarget.id.split('-')[2] && e.currentTarget.id.split('-')[2] == 'preview' || e.currentTarget.id == 'modal-msg') {
        console.log('preview');
    } else {
        vm.resetAll();
    }
})
</script>

</html>