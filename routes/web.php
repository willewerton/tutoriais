<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('home');
})->name('home');
Route::get('/another', function () {
    return 'oooooooooooooooooooooooooooooi';
})->name('another');

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', ['uses' => 'TutorialController@index', 'as' => 'tutorial.index']);
Route::post('/getflows/', ['uses' => 'TutorialController@flows', 'as' => 'tutorial.flows']);
Route::post('/flow', ['uses' => 'TutorialController@addFlow', 'as' => 'tutorial.addflow']);
Route::put('/flow/step/{id}', ['uses' => 'TutorialController@addStep', 'as' => 'tutorial.addstep']);
Route::delete('/flow/remove/{id}', ['uses' => 'TutorialController@remove', 'as' => 'tutorial.remove']);
Route::delete('/flow/step/remove/{id}', ['uses' => 'TutorialController@removeStep', 'as' => 'tutorial.removestep']);


